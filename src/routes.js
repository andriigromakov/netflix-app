export const HOME = '/';
export const BROWSE = '/browse';
export const SIGN_UP = '/signup';
export const LOGIN = '/login';
export const NOT_FOUND = '*';
export const MY_LIST = '/my-list'
export const YOUR_ACCOUNT = '/YourAccount'
export const FORGOT_PASSWORD = '/forgot-password';
export const CHANGE_EMAIL = '/change-email';
export const CHANGE_NAME = '/change-name';
export const CHANGE_PASSWORD = '/change-password';