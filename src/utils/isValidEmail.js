function isValidEmail(email) {
  return /^[\w.]+@+\w+\.\w+$/.test(email);
}

export default isValidEmail;