function notEmpty(name) {
  return Boolean(name.length);
}

export default notEmpty;