function isValidPassword(password) {
  return /^.{6,60}$/.test(password);
}

export default isValidPassword;