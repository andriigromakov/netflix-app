import React from 'react';
import { BrowseContainer } from '../components';
import styles from './theme.module.css';

const MyList = () => {
  return (
    <BrowseContainer
      className={styles.dark}
      title="My List"
      searchSelector={true}
      yearSelector={false}
      genreSelector={false}
      myList={true}
    />
  );
};

export default MyList;