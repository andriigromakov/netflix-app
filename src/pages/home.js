import React from "react";
import { JumbotronContainer, Footer, FAQ, HeaderHomePage } from "../components/index";
import styles from './theme.module.css';
const Home = ({setSignupEmail, signupEmail}) => (
  <div className={styles.dark}>
    <HeaderHomePage setSignupEmail={setSignupEmail} signupEmail={signupEmail}/>
    <JumbotronContainer />
    <FAQ setSignupEmail={setSignupEmail} signupEmail={signupEmail}/>
    <Footer linksShort={false} copyright={true}/>
  </div>
);
export default Home;
