import React from "react";
import { Header, Footer, LoginForm } from "../components/index";
import styles from './theme.module.css';
const Login = ({signupEmail}) => {
  return (
    <div className={styles.dark}>
      <Header bgSmall={false}>
        <LoginForm signupEmail={signupEmail}/>
      </Header>
      <Footer />
    </div>
  );
};

export default Login;
