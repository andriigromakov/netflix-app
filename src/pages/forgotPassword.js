import React from "react";
import { ForgotPasswordForm, Header } from "../components/index";
import styles from "./theme.module.css";
import helpers from "./helpers.module.css";
const ForgotPassword = () => {
  return (
    <div className={`${styles.light} ${helpers['d-flex']} ${helpers['flex-direction-col']}`}>
      <Header linkToLogin={true} bg={false} className={helpers["bg-black"]} />
      <div className={`${helpers['margin-t']}` }>
        <ForgotPasswordForm />
      </div>
    </div>
  );
};

export default ForgotPassword;
