import React from 'react';
import { Header, Footer, Account } from '../components';
import styles from './theme.module.css';
import helpers from './helpers.module.css';

const YourAccount = () => {
  return (
    <div className={styles.light}>
      <Header nav={true} menu={true} className={helpers['bg-black']}/>
      <Account />
      <Footer />
    </div>
  );
};

export default YourAccount;