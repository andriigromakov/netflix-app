import React from "react";
import { Link } from "react-router-dom";
import { Header } from "../components/index";
import * as ROUTES from "../routes";
import theme from "./theme.module.css";
import styles from "./notFound.module.css";
const NotFound = () => {
  return (
    <div className={`${theme.dark} ${styles.wrapper}`}>
      <Header bg={false}/>
      <div style={{
        flexGrow: '1',
        backgroundImage: 'url(/images/404/404-bg.png)',
        backgroundSize: 'cover',
        backgroundPosition: 'center bottom',        
      }}>
        <h1 className={styles.title}>Lost your way?</h1>
        <h2 className={styles.subtitle}>
          Sorry, we can't find that page. You'll find lots to explore on the
          home page
        </h2>
        <Link to={ROUTES.HOME} className={styles.return}>Netflix Home</Link>
      </div>
    </div>
  );
};

export default NotFound;
