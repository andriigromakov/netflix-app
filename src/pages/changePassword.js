import React from "react";
import { ChangePasswordForm, Header } from "../components/index";
import styles from "./theme.module.css";
import helpers from "./helpers.module.css";
const ChangePassword = () => {
  return (
    <div className={`${styles.light}`}>
      <Header nav={true} menu={true} bg={false} className={helpers["bg-black"]} />
      <div className={`${helpers['margin-t']} ${helpers['padding-x-1']}` }>
        <ChangePasswordForm />
      </div>
    </div>
  );
};

export default ChangePassword;
