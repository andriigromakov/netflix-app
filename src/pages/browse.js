import React from "react";
import { BrowseContainer } from '../components';
import styles from "./theme.module.css";

const Browse = () => {
  return (
    <BrowseContainer
      className={styles.dark}
      title="TV Shows"
      searchSelector={true}
      yearSelector={true}
      genreSelector={true}
    />
  );
};

export default Browse;
