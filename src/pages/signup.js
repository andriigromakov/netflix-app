import React from "react";
import styles from "./theme.module.css";
import helpers from './helpers.module.css';
import {Header, Footer, SignupForm} from '../components/index'
const Signup = ({signupEmail}) => {
  return <div className={styles.light}>
  <Header bg={false} linkToLogin={true} linkToLoginAsLink={true} className={helpers['border-b-l-grey']}/>
  <SignupForm signupEmail={signupEmail}/>
  <Footer />
  </div>;
};
export default Signup;
