import React from "react";
import { ChangeEmailForm, Header } from "../components/index";
import styles from "./theme.module.css";
import helpers from "./helpers.module.css";
const ChangeEmail = () => {
  return (
    <div className={`${styles.light}`}>
      <Header nav={true} menu={true} bg={false} className={helpers["bg-black"]} />
      <div className={`${helpers['margin-t']} ${helpers['padding-x-1']}`}>
        <ChangeEmailForm />
      </div>
    </div>
  );
};

export default ChangeEmail;
