import React, { createContext, useContext, useState, useEffect } from "react";
import { auth } from "../lib/firebase";
import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
  sendPasswordResetEmail,
  updatePassword,
  updateProfile,
  updateEmail,
  deleteUser,
} from "firebase/auth";
import {
  getDataFromCollection,
  addDocumentToCollection,
  getSingleUserData,
} from "../lib/firebase";


async function initializeSeries(setSeriesList) {
  //alternative way to initialize state, to see  comment out rest of the function body
  // let res = await getSeriesFromApi("https://api.tvmaze.com/shows");
  // setSeriesList(res);
  let res;
  res = await getDataFromCollection("series", "movieId");
  if (res.length) {
    setSeriesList(res);
  } else {
    res = await getSeriesFromApi("https://api.tvmaze.com/shows");
    res.forEach(async (item) => {
      await addDocumentToCollection("series", item);
    });
    res = await getDataFromCollection("series", "movieId");
    setSeriesList(res);
  }

}

async function getSeriesFromApi(url) {
  try {
    let result = await fetch(url);
    result = await result.json();
    result = 
    result.map(
      ({ id, image, language, name, premiered, rating, summary, genres }) => ({
        movieId: id,
        image,
        language,
        name,
        rating: rating.average,
        summary: summary.replace(/<.*?>/g, ""),
        year: +premiered.substring(0, 4),
        genres,
        likes: 0,
        dislikes: 0
      })
    )
    .filter(item => item.genres.length && item.image.medium && item.image.original);
    return result;
  } catch (error) {
    console.log(error);
  }
}

const FirebaseContext = createContext(null);

export function useFirebaseContext() {
  return useContext(FirebaseContext);
}

export function FirebaseContextPovider({ children }) {
  const [currentUser, setCurrentUser] = useState();
  const [loading, setLoading] = useState(true);
  const [seriesList, setSeriesList] = useState([]);
  const [selectedList, setSelectedList] = useState([]);
  const [likedList, setLikedList] = useState([]);
  const [dislikedList, setDislikedList] = useState([]);
  const [userDataId, setUserDataId] = useState('');
  useEffect(() => {
    initializeSeries(setSeriesList);
  }, []);

  function signup(email, password) {
    return createUserWithEmailAndPassword(auth, email, password);
  }

  function login(email, password) {
    return signInWithEmailAndPassword(auth, email, password);
  }

  function logout() {
    return signOut(auth);
  }

  function resetPassword(email) {
    return sendPasswordResetEmail(auth, email)
  }

  function updateUserPassword(password) {
    return updatePassword(auth.currentUser, password);
  }

  function updateUserName(name) {
    return updateProfile(auth.currentUser, {
      displayName: name
    });
  }

  function updateUserEmail(email) {
    return updateEmail(auth.currentUser, email)
  }

  function deleteUserAccount () {
    return deleteUser(auth.currentUser)
  }

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setCurrentUser(user);
      setLoading(false);
      
    });

    return unsubscribe;
  }, []);

  async function fillUserData(email) {
    let userData = await getSingleUserData(email);
    setUserDataId(userData.firebaseId);
    setLikedList(userData.liked);
    setDislikedList(userData.disliked);
    setSelectedList(userData.selected);

  }

  useEffect(() => {
    if (!currentUser) {
      return;
    }
    fillUserData(currentUser.email)
    
  }, [currentUser])

  const value = {
    selectedList, 
    setSelectedList,
    likedList, 
    setLikedList,
    dislikedList, 
    setDislikedList,
    seriesList,
    setSeriesList,
    currentUser,
    signup,
    login,
    logout,
    resetPassword,
    updateUserPassword,
    updateUserName,
    updateUserEmail,
    deleteUserAccount,
    userDataId,
    loading,
    setUserDataId
  };

  return (
    <FirebaseContext.Provider value={value}>
      {children}
    </FirebaseContext.Provider>
  );
}
