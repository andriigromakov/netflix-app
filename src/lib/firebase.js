import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

import {
  getFirestore,
  collection,
  query,
  addDoc,
  getDocs,
  getDoc,
  setDoc,
  orderBy,
  where,
  doc,
  deleteDoc,
} from "firebase/firestore";

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const db = getFirestore();

export async function getDataFromCollection(
  collectionName,
  sortBy = null,
  desc = false
) {
  const res = [];
  try {
    const collectionRef = collection(db, collectionName);
    let q;
    if (sortBy && desc) {
      q = query(collectionRef, orderBy(sortBy, "desc"));
    } else if (sortBy) {
      q = query(collectionRef, orderBy(sortBy));
    } else {
      q = query(collectionRef);
    }

    const querySnapshot = await getDocs(q);

    querySnapshot.forEach((doc) => {
      let item = {
        firebaseId: doc.id,
        ...doc.data(),
      };
      res.push(item);
    });

    return res;
  } catch (error) {
    console.log(error);
  }
}

export async function getSingleDocument(col, id) {
  try {
    const docRef = doc(db, col, id);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      return docSnap.data();
    } else {
      throw new Error("No such document!");
    }
  } catch (error) {
    console.log(error);
  }
}

export async function setSingleDocumentField(col, id, fieldName, fieldValue) {
  try {
    const colRef = doc(db, col, id);
    const update = {
      [fieldName]: fieldValue,
    };
    await setDoc(colRef, update, { merge: true });
  } catch (error) {
    console.log(error);
  }
  return "userData with id " + id + " is updated";
}

export async function incrementDocField(col, firebaseId, fieldName) {
  try {
    const currentDoc = await getSingleDocument(col, firebaseId);
    let fieldValue = currentDoc[fieldName];
    await setSingleDocumentField(col, firebaseId, fieldName, ++fieldValue);
    // console.log('document with id: ' + firebaseId + ' field: ' + fieldName +  ' is incremented')
  } catch (error) {
    console.log(error);
  }
}

export async function decrementDocField(col, firebaseId, fieldName) {
  try {
    const currentDoc = await getSingleDocument(col, firebaseId);
    let fieldValue = currentDoc[fieldName];
    if (fieldValue > 0) {
      await setSingleDocumentField(col, firebaseId, fieldName, --fieldValue);
      // console.log('document with id: ' + firebaseId + ' field: ' + fieldName +  ' is incremented');
    } else {
      // console.log('document with id: ' + firebaseId + ' field: ' + fieldName +  ' is already 0');
    }
  } catch (error) {
    console.log(error);
  }
}

export async function getSingleUserData(email) {
  const res = [];
  try {
    const q = query(collection(db, "users"), where("email", "==", email));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      let item = {
        firebaseId: doc.id,
        ...doc.data(),
      };
      res.push(item);
    });
    return res[0];
  } catch (error) {
    console.log(error);
  }
}

export async function updateUserDataList(id, fieldName, fieldValue) {
  try {
    const colRef = doc(db, "users", id);
    const update = {
      [fieldName]: fieldValue,
    };
    setDoc(colRef, update, { merge: true });
  } catch (error) {
    console.log(error);
  }
  return "userData with id " + id + " is updated";
}

export async function addDocumentToCollection(collectionName, document) {
  try {
    const docRef = await addDoc(collection(db, collectionName), document);
    return "Document written with ID: " + docRef.id;
  } catch (e) {
    console.error("Error adding document: ", e);
  }
}

export async function deleteDocumentById(collectionName, id) {
  await deleteDoc(doc(db, collectionName, id));
}

export default app;
