import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import * as ROUTES from "./routes";
import { Home, Browse, Login, Signup, NotFound, MyList, YourAccount, ForgotPassword, ChangePassword, ChangeName, ChangeEmail } from "./pages/index";
import { PublicOnlyRoute, PrivatedRoute } from "./helpers/routes";
import { useFirebaseContext } from "./context/FirebaseContext";

function App() {
  const [signupEmail, setSignupEmail] = useState("");
  const { currentUser } = useFirebaseContext();

  return (
    <Router>
      <Switch>
        <PublicOnlyRoute user={currentUser} loggedInPath={ROUTES.BROWSE} exact path={ROUTES.HOME}>
          <Home setSignupEmail={setSignupEmail} signupEmail={signupEmail} />
        </PublicOnlyRoute>
        <PrivatedRoute user={currentUser} loggedInPath={ROUTES.HOME} exact path={ROUTES.BROWSE}>
          <Browse />
        </PrivatedRoute>
        
        <PrivatedRoute user={currentUser} loggedInPath={ROUTES.HOME} exact path={ROUTES.MY_LIST}>
          <MyList />
        </PrivatedRoute>

        <PrivatedRoute user={currentUser} loggedInPath={ROUTES.HOME} exact path={ROUTES.YOUR_ACCOUNT}>
          <YourAccount />
        </PrivatedRoute>

          <PublicOnlyRoute user={currentUser} loggedInPath={ROUTES.BROWSE} exact path={ROUTES.SIGN_UP}>
            <Signup signupEmail={signupEmail} />
          </PublicOnlyRoute>


          <PublicOnlyRoute user={currentUser} loggedInPath={ROUTES.BROWSE} exact path={ROUTES.LOGIN}>
            <Login signupEmail={signupEmail} />
          </PublicOnlyRoute>
          
          <PublicOnlyRoute user={currentUser} loggedInPath={ROUTES.BROWSE} exact path={ROUTES.FORGOT_PASSWORD}>
            <ForgotPassword />
          </PublicOnlyRoute>
          <PrivatedRoute user={currentUser} loggedInPath={ROUTES.HOME} exact path={ROUTES.CHANGE_PASSWORD}>
            <ChangePassword />
          </PrivatedRoute>
          <PrivatedRoute user={currentUser} loggedInPath={ROUTES.HOME} exact path={ROUTES.CHANGE_NAME}>
            <ChangeName />
          </PrivatedRoute>
          <PrivatedRoute user={currentUser} loggedInPath={ROUTES.HOME} exact path={ROUTES.CHANGE_EMAIL}>
            <ChangeEmail />
          </PrivatedRoute>

        <Route path={ROUTES.NOT_FOUND} component={NotFound} />
      </Switch>
    </Router>
  );
}

export default App;
