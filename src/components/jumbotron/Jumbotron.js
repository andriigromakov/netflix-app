import React from "react";
import styles from "./Jumbotron.module.css";
import {Border} from '../index'

const Jumbotron = ({ flipped, title, subTitle, image, alt, extraSpace }) => {
  const flippedStyle = flipped ? styles.flipped : "";
  const extraSpaceStyle = extraSpace ? styles['extra-space'] : "";
  return (
    <>
    <div className={`${styles.pane} ${styles.inner} ${flippedStyle}`}>
      <div className={`${styles['pane-grow']} ${styles['pane-text']}`}>
        <h1 className={styles.title}>{title}</h1>
        <h2 className={styles.subtitle}>{subTitle}</h2>
      </div>
      <div className={`${styles.pane} ${styles['pane-grow']} ${styles['pane-image']}`}>
        <img className={`${styles['jumbotron-image']} ${extraSpaceStyle}`} src={image} alt={alt} />
      </div>
    </div>
    <Border />
    </>
  );
};

export default Jumbotron;
