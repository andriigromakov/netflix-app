import React from "react";
import { Link } from "react-router-dom";
import {useFirebaseContext} from "../../context/FirebaseContext"
import * as ROUTES from '../../routes';
import styles from "./headerMenu.module.css";

const HeaderMenu = () => {
  const {logout, currentUser, setUserDataId, setLikedList, setDislikedList, setSelectedList} = useFirebaseContext();
  return (
    <div className={styles.dropdown}>
      <div className={styles.droptop}>
        <img className={styles['user-image']} src="/images/users/user-image.png" alt="user icon"/>
      </div>
      <div className={styles["dropdown-content"]}>
        <span className={styles["dropdown-item-title"]}>{currentUser.displayName || currentUser.email}</span>
        <Link className={styles["dropdown-item"]} to={ROUTES.YOUR_ACCOUNT}>Account</Link>
        <button className={styles["dropdown-item"]} onClick={() => {
          setUserDataId('');
          setLikedList([]);
          setDislikedList([]);
          setSelectedList([]);
          logout();
        }}>Sign Out</button>
      </div>
    </div>
  );
};

export default HeaderMenu;
