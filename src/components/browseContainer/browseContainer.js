import React, { useState } from "react";
import styles from "./browseContainer.module.css";
import { useFirebaseContext } from "../../context/FirebaseContext";
import {
  Header,
  HeaderFeature,
  CardGroup,
  Footer,
  Selector,
  Search,
} from "../index";

function toArray(data) {
  if (Array.isArray(data)) {
    return data;
  }
  const res = [];
  for (const key in data) {
    if (data.hasOwnProperty(key)) {
      const element = data[key];
      element.genre = key;
      res.push(element);
    }
  }
  return res;
}

const filterByGenre = (arr, genre = "") => {
  if (!genre) {
    return [...arr];
  }
  return arr.filter((item) => item.genre === genre);
};

const filterByYear = (arr, year = "") => {
  if (!year) {
    return [...arr];
  }

  return arr.map((genreItem) => {
    let result = genreItem.filter((film) => film.year === +year);
    result.genre = genreItem.genre;
    return result;
  });
};

const filterByTitle = (arr, title = "") => {
  if (!title) {
    return [...arr];
  }
  return arr.map((genreItem) => {
    let result = genreItem.filter((film) =>
      film.name.toLowerCase().includes(title.toLowerCase())
    );
    result.genre = genreItem.genre;
    return result;
  });
};

const BrowseContainer = ({
  className,
  title = "",
  searchSelector = true,
  yearSelector = false,
  genreSelector = false,
  myList = false,
}) => {
  const { seriesList, selectedList } = useFirebaseContext();

  const [genreFilter, setGenreFilter] = useState("");
  const [yearFilter, setYearFilter] = useState("");
  const [searchFilter, setSearchFilter] = useState("");

  let selectedSeriesList = [];
  if (myList) {
    selectedSeriesList = seriesList.filter((el) =>
      selectedList.includes(el.movieId)
    );
    selectedSeriesList.genre = "My List";
    selectedSeriesList = [selectedSeriesList];
  }
  const genresList = seriesList
    .map((el) => el.genres)
    .reduce((acc, cur) => {
      cur.forEach((item) => {
        if (!acc.includes(item)) {
          acc.push(item);
        }
      });
      return acc;
    }, []);

  const yearsList = seriesList
    .reduce((acc, cur) => {
      if (!acc.includes(cur.year)) {
        acc.push(cur.year);
      }
      return acc;
    }, [])
    .sort((a, b) => b - a);

  const seriesByGenre = {};
  if (!myList) {
    genresList.forEach((item) => {
      seriesByGenre[item] = [];
    });
    seriesList.forEach((outer) => {
      outer.genres.forEach((inner) => {
        seriesByGenre[inner].push(outer);
      });
    });
  }

  let slides = myList ? selectedSeriesList : toArray(seriesByGenre);

  if (genreSelector) {
    slides = filterByGenre(slides, genreFilter);
  }

  if (yearSelector) {
    slides = filterByYear(slides, yearFilter);
  }

  if (searchSelector) {
    slides = filterByTitle(slides, searchFilter);
  }

  return (
    <div className={className}>
      <Header bg={false} nav={true} menu={true}>
        <div className={styles.wrapper}>
          <h1 className={styles.title}>{title}</h1>
          {genreSelector && (
            <Selector
              filterValue={genreFilter}
              setFilterValue={setGenreFilter}
              placeholder="Genres"
              options={genresList}
            />
          )}
          {yearSelector && (
            <Selector
              filterValue={yearFilter}
              setFilterValue={setYearFilter}
              placeholder="Year"
              options={yearsList}
            />
          )}
          {searchSelector && (
            <Search
              placeholder="enter title to search"
              searchValue={searchFilter}
              setSearchValue={setSearchFilter}
            />
          )}
        </div>
        {/* <HeaderFeature movie={seriesList[0]} /> */}
      </Header>
      <div>
        <CardGroup
          data={slides}
          isOpen={myList || genreFilter || searchFilter ? true : false}
        />
      </div>
      <Footer linksShort={false} copyright={true} />
    </div>
  );
};

export default BrowseContainer;
