import React, { useState } from "react";
import styles from "./RegistrationForm.module.css";
import isValidEmail from "../../utils/isValidEmail";
import { useHistory } from "react-router-dom";

const RegistrationForm = ({
  title,
  placeholder,
  btnText,
  id,
  className,
  setSignupEmail,
  signupEmail,
}) => {
  const [inputActive, setInputActive] = useState(signupEmail ? true : false);
  if (signupEmail && !inputActive) {
    setInputActive(true);
  }
  const [validEmail, setValidEmail] = useState(true);
  // const [emailValue, setEmailValue] = useState(signupEmail || "");
  const history = useHistory();
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!isValidEmail(signupEmail)) {
      setValidEmail(false);
      return;
    }
    // setSignupEmail(emailValue);
    history.push("/signup");
  };

  return (
    <div className={`${styles.wrapper} ${className}`}>
      <h3 className={styles.title}>{title}</h3>
      <form className={styles.form} autoComplete="off" onSubmit={handleSubmit}>
        <span className={styles["input-wrapper"]}>
          <input
            onFocus={() => setInputActive(true)}
            onBlur={() => signupEmail === "" && setInputActive(false)}
            onChange={(e) => {
              // setEmailValue(e.target.value);
              setValidEmail(isValidEmail(e.target.value));
              setSignupEmail(e.target.value);
              setInputActive(true);
            }}
            className={styles["input-control"]}
            id={"id_email_" + id}
            type="text"
            value={signupEmail}
          />
          <label
            className={`${styles.placelabel} ${
              inputActive ? styles["placelabel-active"] : ""
            }`}
            htmlFor={"id_email_" + id}
          >
            {placeholder}
          </label>
          <span
            className={`${styles["input-error"]} ${
              styles[!validEmail ? "input-error-active" : ""]
            }`}
          >
            Email is required!
          </span>
        </span>

        <button className={styles.btn} type="submit">
          {btnText}
          <svg
            className={styles.shevron}
            viewBox="0 0 6 12"
            xmlns="http://www.w3.org/2000/svg"
            fill="white"
          >
            <desc>chevron</desc>
            <path
              d="M.61 1.312l.78-.624L5.64 6l-4.25 5.312-.78-.624L4.36 6z"
              fill="white"
              fillRule="evenodd"
            ></path>
          </svg>
        </button>
      </form>
    </div>
  );
};

export default RegistrationForm;
