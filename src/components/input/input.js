import React from 'react'
import styles from './input.module.css'

const InputLabel = ({ active, text, id }) => {
  return (
    <label
      className={`${styles.placelabel} ${
        active ? styles["placelabel-active"] : ""
      }`}
      htmlFor={id}
    >
      {text}
    </label>
  );
};

export default function Input({
  inputPlaceholder,
  errorPlaceholder,
  validInput,
  setValidInput,
  inputValue,
  setInputValue,
  inputValidator,
  inputActive,
  setInputActive,
  id,
  type = "text",
  className = ''
}) {
  return (
    <div className={`${styles["input-wrapper"]} ${className}`}>
      <input
        onFocus={() => setInputActive(true)}
        onBlur={() => {
          if (!inputValue) {
            setInputActive(false);
          }
        }}
        onChange={(e) => {
          setValidInput(inputValidator(e.target.value));
          setInputValue(e.target.value);
        }}
        className={styles["input-control"]}
        id={id}
        type={type}
        value={inputValue}
      />
      <InputLabel active={inputActive} text={inputPlaceholder} id={id} />
      <span
        className={`${styles["input-error"]} ${
          styles[!validInput ? "input-error-active" : ""]
        }`}
      >
        {errorPlaceholder}
      </span>
    </div>
  );
};