import React, {useState} from "react";
import { Link, useHistory } from "react-router-dom";
import { useFirebaseContext } from "../../context/FirebaseContext";
import { deleteDocumentById } from '../../lib/firebase';
import * as ROUTES from "../../routes";
import styles from "./account.module.css";

const Account = () => {
  const { currentUser, deleteUserAccount, userDataId, setUserDataId, setLikedList, setDislikedList, setSelectedList } = useFirebaseContext();

  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const deleteAcount = async () => {
    try {
      setError('');
      setLoading(true);     


      await deleteUserAccount();

      await deleteDocumentById('users', userDataId);

      setUserDataId('');
      setLikedList([]);
      setDislikedList([]);
      setSelectedList([]);

      history.push(ROUTES.HOME);
    } catch (err) {
      console.log(err);
      setError('Failed to delete account. : ' + err.message + ' Try to sign out and login again.');
      setLoading(false);
    }
  };

  return (
    <div className={styles.wrapper}>
      <h1>Account</h1>
      {error && <div className={styles.error}>{error}</div>}
      <span className={styles['delete-wrapper']}>
        <button className={styles.btn} disabled={loading} onClick={deleteAcount}>Delete Account</button>
        <span className={styles.warn}>If you delete account it cannot be undone!</span>
      </span>
      <div className={styles.innerWrapper}>
        <div className={styles.email}>Email: {currentUser.email}</div>
        <div>
          <Link className={styles.link} to={ROUTES.CHANGE_EMAIL}>
            Change Email
          </Link>
        </div>
        <div>Name: {currentUser.displayName || ""}</div>
        <div>
          <Link className={styles.link} to={ROUTES.CHANGE_NAME}>
            Change Name
          </Link>
        </div>
        <div>Password: ******</div>
        <div>
          <Link className={styles.link} to={ROUTES.CHANGE_PASSWORD}>
            Change Password
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Account;
