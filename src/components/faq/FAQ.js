import React from "react";
import { Accordion, RegistrationForm } from "../index";
import Border from "../border/border";
import styles from "./FAQ.module.css";
import faqData from "../../fixtures/faq.json";
const FAQ = ({ setSignupEmail, signupEmail }) => (
  <>
    <h2 className={styles.title}>Frequently Asked Questions</h2>
    <Accordion items={faqData} />
    <RegistrationForm
      title="Ready to watch? Enter your email to create or restart your membership."
      placeholder="Email address"
      btnText="Get Started"
      id="faq"
      className={styles.form}
      setSignupEmail={setSignupEmail}
      signupEmail={signupEmail}
    />
    <Border />
  </>
);
export default FAQ;
