import React, { useState } from "react";
import {useHistory} from 'react-router-dom';
import * as ROUTES from '../../routes'
import { Input } from "../index";
import isValidEmail from "../../utils/isValidEmail";
import { useFirebaseContext } from "../../context/FirebaseContext";
import {setSingleDocumentField} from '../../lib/firebase'
import styles from "./changeEmailForm.module.css";

const ChangeEmailForm = () => {
  const [validEmail, setValidEmail] = useState(true);
  const [emailValue, setEmailValue] = useState("");
  const [emailActive, setEmailActive] = useState(false);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const {updateUserEmail, userDataId} = useFirebaseContext();
  const history = useHistory();
  
  const submitHandler = async (e) => {
    e.preventDefault();
    if (!isValidEmail(emailValue)) {
      setError('Email is required!');
      return;
    }
    try {
      setError('');
      setLoading(true);     

      await updateUserEmail(emailValue);

      await setSingleDocumentField('users', userDataId, 'email', emailValue);

      setLoading(false);
      history.push(ROUTES.YOUR_ACCOUNT);
    } catch(err) {
      console.log(err);
      setError('Failed to update name. : ' + err.message + ' Try to sign out and login again.');
      setLoading(false);
    }

  }

  return (
    <div>
      <form className={styles.form} onSubmit={submitHandler}>
        {error && <div className={styles.error}>{error}</div>}
        <Input
          inputPlaceholder="new email"
          errorPlaceholder="Email is required!"
          validInput={validEmail}
          setValidInput={setValidEmail}
          inputValue={emailValue}
          setInputValue={setEmailValue}
          inputValidator={isValidEmail}
          inputActive={emailActive}
          setInputActive={setEmailActive}
          id="change-email"
          type="text"
          className={styles["margin-b"]}
        />
        <button className={styles.btn} type="submit" disabled={loading}>
          Update Email
        </button>
      </form>
    </div>
  );
};

export default ChangeEmailForm;
