import React from 'react';
import styles from './headerFeature.module.css';
const HeaderFeature = ({movie}) => {
  console.log(movie.image.original);
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      backgroundImage: `url(${movie.image.original})`,
      backgroundSize: 'contain',
      backgroundPosition: 'right',
      backgroundRepeat: 'no-repeat',
      height: "80vmin",
      overflow: 'hidden'
    }}>
      <h2 className={styles.title}>{movie.name}</h2>
      <p className={styles.summary}>{movie.summary}</p>
      
    </div>
  );
};

export default HeaderFeature;