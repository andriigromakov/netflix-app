// import { collectionGroup } from '@firebase/firestore';
import React, {useState} from 'react';
import styles from './selector.module.css';
const Selector = ({className = '', placeholder = '', setFilterValue, filterValue, options}) => {
  const [isOpen, setIsOpen] = useState(false);
  const resetFilter = (e) => {
    e.stopPropagation();
    setIsOpen(false);
    setFilterValue('')
  };

  return (
    <div className={`${styles.dropdown} ${className}`}>
      <div className={styles['droptop-wrapper']}>
        <div
        className={styles.droptop} onClick={() => {
        setIsOpen((state) => !state)
      }}
        >{filterValue || placeholder}</div>
        {<div className={`${styles["close-btn"]} ${filterValue ? styles["close-btn-active"] : ''}`} onClick={resetFilter}>
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="fas"
              data-icon="times"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 352 512"
            >
              <path
                d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"
              ></path>
            </svg>
          </div>}
      </div>
      <div className={`${styles["dropdown-content"]} ${isOpen ? styles["dropdown-content-active"]: ''}`}>
        {options.map(item => <div key={item} onClick={e => {
          setIsOpen(false);
          setFilterValue(e.target.textContent);
        }} className={styles["dropdown-item"]}>{item}</div>)}
      </div>
    </div>
  );
};

export default Selector;