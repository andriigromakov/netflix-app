import React, { useState } from "react";
import styles from "./forgotPasswordForm.module.css";
import { Input } from "../index";
import isValidEmail from "../../utils/isValidEmail";
import { useFirebaseContext } from "../../context/FirebaseContext";

const ForgotPasswordForm = () => {
  const [validEmail, setValidEmail] = useState(true);
  const [emailValue, setEmailValue] = useState("");
  const [emailActive, setEmailActive] = useState(false);

  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const [error, setError] = useState('');

  const {resetPassword} = useFirebaseContext();
  
  const submitHandler = async (e) => {
    e.preventDefault();
    if (!validEmail || !emailValue) {
      return
    }
    try {
      setMessage('')
      setError('');
      setLoading(true);
      await resetPassword(emailValue);
      setMessage('Check your email inbox for further instructions')
      setLoading(false);
    } catch(err) {
      console.log(err);
      setError('Failed to reset password: ' + err.message);
      setLoading(false);
    }
  }

  return (
    <div>
      <form className={styles.form} onSubmit={submitHandler}>
      {error && <div className={styles.error}>{error}</div>}
      {message && <div className={styles.message}>{message}</div>}
        <Input
          inputPlaceholder="Enter your Email"
          errorPlaceholder="Email is required!"
          validInput={validEmail}
          setValidInput={setValidEmail}
          inputValue={emailValue}
          setInputValue={setEmailValue}
          inputValidator={isValidEmail}
          inputActive={emailActive}
          setInputActive={setEmailActive}
          id="forgot-password-value"
          type="text"
          className={styles["margin-b"]}
        />
        <button className={styles.btn} type="submit" disabled={loading}>
          Reset Email
        </button>
      </form>
    </div>
  );
};

export default ForgotPasswordForm;
