import React from 'react';
import { NavLink } from 'react-router-dom'
import * as ROUTES from '../../routes'
import styles from './nav.module.css';

const Nav = ({className}) => {
  return (
    <div className={className || ''}>
      <NavLink activeClassName={styles['link-active']} className={styles.link} to={ROUTES.BROWSE}>Home</NavLink>
      <NavLink activeClassName={styles['link-active']} className={styles.link} to={ROUTES.MY_LIST}>My List</NavLink>
    </div>
  );
};

export default Nav;