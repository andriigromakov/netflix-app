import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useFirebaseContext } from "../../context/FirebaseContext";
import * as ROUTES from "../../routes";
import isValidEmail from "../../utils/isValidEmail";
import isValidPassword from "../../utils/isValidPassword";
import { Input } from "../index";

import styles from "./loginForm.module.css";

const LoginForm = ({signupEmail}) => {
  const [validEmail, setValidEmail] = useState(true);
  const [emailValue, setEmailValue] = useState("");
  const [emailActive, setEmailActive] = useState(false);
  const [validPassword, setValidPassword] = useState(true);
  const [passwordValue, setPasswordValue] = useState("");
  const [passwordActive, setPasswordActive] = useState(false);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  const { login } = useFirebaseContext();
  const history = useHistory();
  const isSignUpEmailValid = isValidEmail(signupEmail);

  const submitHandler = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (!validEmail || !validPassword) {
      return;
    }
    try {
      setError("");
      setLoading(true);
      await login(emailValue, passwordValue);
      history.push(ROUTES.BROWSE);
    } catch (error) {
      setLoading(false);
      console.log(error);
      setEmailValue("");
      setPasswordValue("");
      setError("Failed to sign in: " + error.message);
    }
  };

  return (
    <div className={styles.wraper}>
      <form onSubmit={submitHandler} className={styles.form} autoComplete="off">
        <h1 className={styles.title}>Sign In</h1>
        {error && <div className={styles.error}>{error}</div>}
        <Input
          id="Login_Form_Email"
          inputPlaceholder={"Email"}
          errorPlaceholder={"Email is required!"}
          validInput={validEmail}
          setValidInput={setValidEmail}
          inputValue={emailValue}
          setInputValue={setEmailValue}
          inputValidator={isValidEmail}
          inputActive={emailActive}
          setInputActive={setEmailActive}
          className={styles['input-control-dark']}
        />
        <Input
          id="Login_Form_Password"
          inputPlaceholder={"Password"}
          errorPlaceholder={
            "Your password must contain between 6 and 60 characters."
          }
          validInput={validPassword}
          setValidInput={setValidPassword}
          inputValue={passwordValue}
          setInputValue={setPasswordValue}
          inputValidator={isValidPassword}
          inputActive={passwordActive}
          setInputActive={setPasswordActive}
          type="password"
          className={styles['input-control-dark']}
        />
        <button className={styles.btn} type="submit" disabled={loading}>
          Sign In
        </button>
        <p><Link className={styles.link} to={ROUTES.FORGOT_PASSWORD}>Forgot password?</Link></p>
        <p>
          New to Netflix?{" "}
          <Link className={styles.link} to={isSignUpEmailValid ? ROUTES.SIGN_UP : ROUTES.HOME}>
            Sign up now
          </Link>
          .
        </p>
      </form>
    </div>
  );
};

export default LoginForm;
