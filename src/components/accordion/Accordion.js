import React, { useState } from "react";
import AccordionItem from "../accordionItem/AccordionItem";
import styles from "./Accordion.module.css";

const Accordion = ({ items }) => {
  const [activeItem, setActiveItem] = useState("");

  const renderedList = items.map((item) => {
    const isActive = item.key === activeItem ? true : false;
    return (
      <AccordionItem
        header={item.header}
        content={item.content}
        isActive={isActive}
        key={item.key}
        onClick={() => {
          item.key === activeItem ? setActiveItem("") : setActiveItem(item.key);
        }}
      />
    );
  });

  return <ul className={styles.list}>{renderedList}</ul>;
};
export default Accordion;
