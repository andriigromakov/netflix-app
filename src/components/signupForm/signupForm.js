import React, { useState } from "react";
import styles from "./signupForm.module.css";
import { Input } from "../index";
import { useHistory } from "react-router-dom";
import { useFirebaseContext } from "../../context/FirebaseContext";
import { addDocumentToCollection } from '../../lib/firebase'
import isValidPassword from "../../utils/isValidPassword";
import isValidEmail from "../../utils/isValidEmail";
import * as ROUTES from "../../routes";

const SignupForm = ({ signupEmail }) => {
  const [validPassword, setValidPassword] = useState(true);
  const [passwordValue, setPasswordlValue] = useState("");
  const [passwordActive, setPasswordlActive] = useState(false);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  const { signup } = useFirebaseContext();
  const history = useHistory();

  const submitHandler = async (e) => {
    e.preventDefault();
    if (!validPassword) {
      setError("Invalid password");
      return;
    }
    if ( !isValidEmail(signupEmail)) {
      setError("Invalid email");
      return;
    }

    let document = {
      email: signupEmail,
      liked: [],
      disliked: [],
      selected: []
    };

    try {
      setError("");
      setLoading(true);
      await signup(signupEmail, passwordValue);
      await addDocumentToCollection('users', document);
      history.push(ROUTES.BROWSE);
    } catch (error) {
      console.log(error);
      setError("Failed to create an account " + error.message);
      setLoading(false);
    }    
  };

  return (
    <div className={styles.wrapper}>
      <p>
        <span className={styles.title}>Welcome back!</span>
        <span className={styles.subtitle}>Joining Netflix is easy.</span>
      </p>
      {error && <div className={styles.error}>{error}</div>}
      

      <p className={styles.desc}>
        Enter your password and you'll be watching in no time.
      </p>
      <p>
        <span className={styles.email}>Email</span>
        <span className={styles["email-value"]}>{signupEmail}</span>
      </p>
      <form onSubmit={submitHandler}>
        <Input
          inputPlaceholder="Password"
          errorPlaceholder="Password should be between 6 and 60 characters"
          validInput={validPassword}
          setValidInput={setValidPassword}
          inputValue={passwordValue}
          setInputValue={setPasswordlValue}
          inputActive={passwordActive}
          setInputActive={setPasswordlActive}
          inputValidator={isValidPassword}
          id="registration-password"
          type="password"
          className={styles["input-control"]}
        />
        <button type="submit" className={styles.btn} disabled={loading}>
          Start Membership
        </button>
      </form>
    </div>
  );
};

export default SignupForm;
