import React from "react";
import styles from "./AccordionItem.module.css";
const AccordionItem = ({ header, content, isActive, onClick }) => {
  return (
    <li className={styles.frame}>
      <button className={styles.header} onClick={onClick}>
        {header}
        <svg viewBox="0 0 26 26" className={`${styles.cross} ${styles[isActive ? '' :'cross-open']}`} fill="white" focusable="true">
          <path d="M10.5 9.3L1.8 0.5 0.5 1.8 9.3 10.5 0.5 19.3 1.8 20.5 10.5 11.8 19.3 20.5 20.5 19.3 11.8 10.5 20.5 1.8 19.3 0.5 10.5 9.3Z"></path>
        </svg>
      </button>
      <div
        className={`${styles.content} ${
          isActive ? styles.open : styles.closed
        }`}
      >
        <span className={styles.inner}>{content}</span>{" "}
      </div>
    </li>
  );
};

export default AccordionItem;
