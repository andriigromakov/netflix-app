import React from "react";
import { Header, RegistrationForm, Border } from "../index";

import styles from './headerHomePage.module.css';

const HeaderHomePage = ({setSignupEmail, signupEmail}) => {
  return <Header linkToLogin={true}>
      <h1 className={styles.title}>Unlimited movies, TV shows, and more.</h1>
      <h2 className={styles.subtitle}>Watch anywhere. Cancel anytime.</h2>
      <RegistrationForm
        title="Ready to watch? Enter your email to create or restart your membership."
        placeholder="Email address"
        btnText="Get Started"
        id="faq"
        className={styles.form}
        setSignupEmail={setSignupEmail}
        signupEmail={signupEmail}
      />
      <Border />
  </Header>;
};

export default HeaderHomePage;
