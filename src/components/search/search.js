import React from 'react';
import styles from './search.module.css';
const Search = ({searchValue = '', setSearchValue, placeholder = ''}) => {
  const onChangeHandler = e => {
    e.preventDefault();
    setSearchValue(e.target.value);
  }
  return (
    <input value={searchValue} onChange={onChangeHandler} placeholder={placeholder} className={styles.search}/>    
  );
};

export default Search;