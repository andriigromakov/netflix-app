import React, { useState } from "react";
import {useHistory} from 'react-router-dom';
import * as ROUTES from '../../routes'
import { Input } from "../index";
import isValidPassword from "../../utils/isValidPassword";
import { useFirebaseContext } from "../../context/FirebaseContext";
import styles from "./changePasswordForm.module.css";

const ChangePasswordForm = () => {
  const [validPassword, setValidPassword] = useState(true);
  const [passwordValue, setPasswordValue] = useState("");
  const [passwordActive, setPasswordActive] = useState(false);
  const [validPasswordConfirm, setValidPasswordConfirm] = useState(true);
  const [passwordConfirmValue, setPasswordConfirmValue] = useState("");
  const [passwordConfirmActive, setPasswordConfirmActive] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const {updateUserPassword} = useFirebaseContext();
  const history = useHistory();
  
  const submitHandler = async (e) => {
    e.preventDefault();
    if (!validPassword || !validPasswordConfirm || !isValidPassword(passwordValue)) {
      return;
    }
    if (passwordValue !== passwordConfirmValue) {
      setError('Passwords do not match');
      return;
    }

    try {
      setError('');
      setLoading(true);
      await updateUserPassword(passwordValue);
      setLoading(false);
      history.push(ROUTES.YOUR_ACCOUNT);
    } catch(err) {
      console.log(err);
      setError('Failed to update password. : ' + err.message + ' Try to sign out and login again.');
      setLoading(false);
    }

  }

  return (
    <div>
      <form className={styles.form} onSubmit={submitHandler}>
        {error && <div className={styles.error}>{error}</div>}
        <Input
          inputPlaceholder="new password"
          errorPlaceholder="Your password must contain between 6 and 60 characters."
          validInput={validPassword}
          setValidInput={setValidPassword}
          inputValue={passwordValue}
          setInputValue={setPasswordValue}
          inputValidator={isValidPassword}
          inputActive={passwordActive}
          setInputActive={setPasswordActive}
          id="change-password-value"
          type="password"
          className={styles["margin-b"]}
        />
        <Input
          inputPlaceholder="confirm new password"
          errorPlaceholder="Your password must contain between 6 and 60 characters."
          validInput={validPasswordConfirm}
          setValidInput={setValidPasswordConfirm}
          inputValue={passwordConfirmValue}
          setInputValue={setPasswordConfirmValue}
          inputValidator={isValidPassword}
          inputActive={passwordConfirmActive}
          setInputActive={setPasswordConfirmActive}
          id="change-password-confirm"
          type="password"
          className={styles["margin-b"]}
        />
        <button className={styles.btn} type="submit" disabled={loading}>
          Change Password
        </button>
      </form>
    </div>
  );
};

export default ChangePasswordForm;
