import React, { useState } from "react";
import {useHistory} from 'react-router-dom';
import * as ROUTES from '../../routes'
import { Input } from "../index";
import notEmpty from "../../utils/notEmpty";
import { useFirebaseContext } from "../../context/FirebaseContext";
import styles from "./changeNameForm.module.css";

const ChangeNameForm = () => {
  const [validName, setValidName] = useState(true);
  const [nameValue, setNameValue] = useState("");
  const [nameActive, setNameActive] = useState(false);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const {updateUserName} = useFirebaseContext();
  const history = useHistory();
  
  const submitHandler = async (e) => {
    e.preventDefault();
    if (!notEmpty(nameValue)) {
      setError('Name should not be empty');
      return;
    }
    try {
      setError('');
      setLoading(true);
      await updateUserName(nameValue);
      setLoading(false);
      history.push(ROUTES.YOUR_ACCOUNT);
    } catch(err) {
      console.log(err);
      setError('Failed to update name. : ' + err.message + ' Try to sign out and login again.');
      setLoading(false);
    }

  }

  return (
    <div>
      <form className={styles.form} onSubmit={submitHandler}>
        {error && <div className={styles.error}>{error}</div>}
        <Input
          inputPlaceholder="new name"
          errorPlaceholder="Name should not be empty"
          validInput={validName}
          setValidInput={setValidName}
          inputValue={nameValue}
          setInputValue={setNameValue}
          inputValidator={notEmpty}
          inputActive={nameActive}
          setInputActive={setNameActive}
          id="change-name"
          type="text"
          className={styles["margin-b"]}
        />
        <button className={styles.btn} type="submit" disabled={loading}>
          Update Name
        </button>
      </form>
    </div>
  );
};

export default ChangeNameForm;
