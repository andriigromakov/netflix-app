import React from "react";
import styles from "./Footer.module.css";
import footerLinksDataFull from "../../fixtures/footerLinksFull.json";
import footerLinksDataShort from "../../fixtures/footerLinksShort.json";
// const links = [
//   { title: "FAQ", href: "#" },
//   { title: "Help Center", href: "#" },
//   { title: "Account", href: "#" },
//   { title: "Media Center", href: "#" },
//   { title: "Investor Relations", href: "#" },
//   { title: "Jobs", href: "#" },
//   { title: "Ways to Watch", href: "#" },
//   { title: "Terms of Use", href: "#" },
//   { title: "Privacy", href: "#" },
//   { title: "Cookie Preferences", href: "#" },
//   { title: "Corporate Information", href: "#" },
//   { title: "Contact Us", href: "#" },
//   { title: "Speed Test", href: "#" },
//   { title: "Legal Notices", href: "#" },
//   { title: "Only on Netflix", href: "#" },
// ];
const Footer = ({ children, linksShort = true, copyright = false, ...restProps }) => {
  const links = linksShort ? footerLinksDataShort : footerLinksDataFull;
  return (
    <div className={styles["inner-wrapper"]}>
      <p className={styles.title}>
        <a href="./" className={styles.link}>
          Questions? Contact us.
        </a>
      </p>
      <ul className={styles.links}>
        {links.map((item, i) => (
          <li key={i} className={styles["link-wrapper"]}>
            <a href={item.href} className={styles.link}>
              {item.title}
            </a>
          </li>
        ))}
      </ul>
      {copyright && <p className={styles.copyright}>Netflix Ukraine</p>}      
    </div>
  );
};

export default Footer;
