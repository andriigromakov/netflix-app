import React from "react";
import jumbotronData from "../../fixtures/jumbotron.json";
import Jumbotron from "../jumbotron/Jumbotron";

const JumbotronContainer = () => {
  return (
    <div>
      {jumbotronData.map((item, i) => (
        <Jumbotron
          key={item.title}
          flipped={item.flipped}
          title={item.title}
          subTitle={item.subTitle}
          image={item.image}
          extraSpace={i === 1 ? false : true}
          alt={item.alt}
        />
      ))}
    </div>
  );
};

export default JumbotronContainer;
