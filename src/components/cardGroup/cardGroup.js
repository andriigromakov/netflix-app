import React, { useState, useEffect } from "react";
import {
  updateUserDataList,
  getSingleDocument,
  setSingleDocumentField
} from "../../lib/firebase";
import { useFirebaseContext } from "../../context/FirebaseContext";
import styles from "./cardGroup.module.css";

export default function CardGroup({ data, isOpen = false }) {
  const [showFeature, setShowFeature] = useState(false);
  const [itemFeature, setItemFeature] = useState({});

  return (
    <div>
      {showFeature && (
        <CardFeature setShowFeature={setShowFeature} item={itemFeature} />
      )}
      {data.map((el) => (
        <Card
          items={el}
          key={el.genre}
          isOpen={isOpen}
          setShowFeature={setShowFeature}
          setItemFeature={setItemFeature}
        />
      ))}
    </div>
  );
}

function CardFeature({ setShowFeature, item }) {
  const {
    selectedList,
    setSelectedList,
    likedList,
    setLikedList,
    dislikedList,
    setDislikedList,
    userDataId,
    setSeriesList
  } = useFirebaseContext();
  const isSelected = selectedList.includes(item.movieId);
  const isLiked = likedList.includes(item.movieId);
  const isDisliked = dislikedList.includes(item.movieId);
  const [selected, setSelected] = useState(isSelected);
  const [liked, setLiked] = useState(isLiked);
  const [disliked, setDisliked] = useState(isDisliked);

  const hideFeature = () => {
    setShowFeature(false);
  };

  const cutFromArray = (arr, value) => {
    const copy = arr.slice();
    const index = copy.findIndex((el) => el === value);
    if (index < 0) {
      return copy;
    }
    copy.splice(index, 1);
    return copy;
  };

  const addToArray = (arr, value) => {
    const copy = arr.slice();
    if (value === undefined) {
      return copy;
    }
    copy.push(value);
    return copy;
  };

  const updateStateToDB = async (list, movieId, userDataId, field) => {
    let res;
    if (list.includes(movieId)) {
      res = cutFromArray(list, movieId);
    } else {
      res = addToArray(list, movieId);
    }
    try {
      await updateUserDataList(userDataId, field, res);
    } catch (error) {
      console.log(error);
    }
  };

  const updateLike = async (firebaseId, likedFlag, dislikedFlag) => {
    const currentDoc = await getSingleDocument("series", firebaseId);
    let likes = currentDoc.likes;
    let dislikes = currentDoc.dislikes;

    if (dislikedFlag && dislikes > 0) {
      await setSingleDocumentField("series", firebaseId, "dislikes", --dislikes);
    }
    if (likedFlag && likes > 0) {
      await setSingleDocumentField("series", firebaseId, "likes", --likes);
    }
    if (!likedFlag) {
      await setSingleDocumentField("series", firebaseId, "likes", ++likes);
    }
    setSeriesList((list) => {
      return list.map((el) => {
        if (el.firebaseId === firebaseId) {
          el.likes = likes;
          el.dislikes = dislikes;
        }
        return el;
      });
    });
  };
  
  const updateDislike = async (firebaseId, likedFlag, dislikedFlag) => {
    const currentDoc = await getSingleDocument("series", firebaseId);
    let likes = currentDoc.likes;
    let dislikes = currentDoc.dislikes;

    if (likedFlag && likes > 0) {
      await setSingleDocumentField("series", firebaseId, "likes", --likes);
    }
    if (dislikedFlag && dislikes > 0) {
      await setSingleDocumentField("series", firebaseId, "dislikes", --dislikes);
    }
    if (!dislikedFlag) {
      await setSingleDocumentField("series", firebaseId, "dislikes", ++dislikes);
    }
    setSeriesList((list) => {
      return list.map((el) => {
        if (el.firebaseId === firebaseId) {
          el.likes = likes;
          el.dislikes = dislikes;
        }
        return el;
      });
    });
  };
  

  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "";
    };
  }, []);

  const style = {
    backgroundImage: `url(${item.image.original})`,
    backgroundSize: "contain",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    borderRadius: "1rem",
  };

  return (
    <div>
      <div className={styles.overlay} onClick={hideFeature}>
        <div
          className={styles["feature-content"]}
          onClick={(e) => e.stopPropagation()}
        >
          <div className={styles["close-btn"]} onClick={hideFeature}>
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="fas"
              data-icon="times"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 352 512"
            >
              <path
                fill="currentColor"
                d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"
              ></path>
            </svg>
          </div>
          <div className={styles["feature-info"]}>
            <h2>{item.name}</h2>
            <div className={styles.actions}>
              <div
                onClick={async () => {
                  setSelected((state) => !state);

                  await updateStateToDB(
                    selectedList,
                    item.movieId,
                    userDataId,
                    "selected"
                  );

                  setSelectedList((list) => {
                    if (list.includes(item.movieId)) {
                      return cutFromArray(list, item.movieId);
                    } else {
                      return addToArray(list, item.movieId);
                    }
                  });
                }}
                className={styles[`feature-icon${selected ? "-active" : ""}`]}
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  data-icon="plus"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 448 512"
                >
                  <path d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
                </svg>
              </div>
              <div
                onClick={async () => {
                  setLiked((state) => !state);

                  await updateLike(item.firebaseId, liked, disliked);

                  await updateStateToDB(
                    likedList,
                    item.movieId,
                    userDataId,
                    "liked"
                  );
                  if (dislikedList.includes(item.movieId)) {
                    await updateStateToDB(
                      dislikedList,
                      item.movieId,
                      userDataId,
                      "disliked"
                    );
                  }

                  setLikedList((list) => {
                    if (list.includes(item.movieId)) {
                      return cutFromArray(list, item.movieId);
                    } else {
                      return addToArray(list, item.movieId);
                    }
                  });
                  if (isDisliked) {
                    setDislikedList((list) => cutFromArray(list, item.movieId));
                    setDisliked((state) => !state);
                  }
                }}
                className={styles[`feature-icon${liked ? "-active" : ""}`]}
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="far"
                  data-icon="thumbs-up"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path d="M466.27 286.69C475.04 271.84 480 256 480 236.85c0-44.015-37.218-85.58-85.82-85.58H357.7c4.92-12.81 8.85-28.13 8.85-46.54C366.55 31.936 328.86 0 271.28 0c-61.607 0-58.093 94.933-71.76 108.6-22.747 22.747-49.615 66.447-68.76 83.4H32c-17.673 0-32 14.327-32 32v240c0 17.673 14.327 32 32 32h64c14.893 0 27.408-10.174 30.978-23.95 44.509 1.001 75.06 39.94 177.802 39.94 7.22 0 15.22.01 22.22.01 77.117 0 111.986-39.423 112.94-95.33 13.319-18.425 20.299-43.122 17.34-66.99 9.854-18.452 13.664-40.343 8.99-62.99zm-61.75 53.83c12.56 21.13 1.26 49.41-13.94 57.57 7.7 48.78-17.608 65.9-53.12 65.9h-37.82c-71.639 0-118.029-37.82-171.64-37.82V240h10.92c28.36 0 67.98-70.89 94.54-97.46 28.36-28.36 18.91-75.63 37.82-94.54 47.27 0 47.27 32.98 47.27 56.73 0 39.17-28.36 56.72-28.36 94.54h103.99c21.11 0 37.73 18.91 37.82 37.82.09 18.9-12.82 37.81-22.27 37.81 13.489 14.555 16.371 45.236-5.21 65.62zM88 432c0 13.255-10.745 24-24 24s-24-10.745-24-24 10.745-24 24-24 24 10.745 24 24z"></path>
                </svg>
              </div>
              <div>{item.likes}</div>
              <div
                onClick={async () => {
                  setDisliked((state) => !state);

                  await updateDislike(item.firebaseId, liked, disliked);

                  await updateStateToDB(
                    dislikedList,
                    item.movieId,
                    userDataId,
                    "disliked"
                  );
                  if (likedList.includes(item.movieId)) {
                    await updateStateToDB(
                      likedList,
                      item.movieId,
                      userDataId,
                      "liked"
                    );
                  }

                  setDislikedList((list) => {
                    if (list.includes(item.movieId)) {
                      return cutFromArray(list, item.movieId);
                    } else {
                      return addToArray(list, item.movieId);
                    }
                  });
                  if (isLiked) {
                    setLikedList((list) => cutFromArray(list, item.movieId));
                    setLiked((state) => !state);
                  }
                }}
                className={styles[`feature-icon${disliked ? "-active" : ""}`]}
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="far"
                  data-icon="thumbs-down"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path d="M466.27 225.31c4.674-22.647.864-44.538-8.99-62.99 2.958-23.868-4.021-48.565-17.34-66.99C438.986 39.423 404.117 0 327 0c-7 0-15 .01-22.22.01C201.195.01 168.997 40 128 40h-10.845c-5.64-4.975-13.042-8-21.155-8H32C14.327 32 0 46.327 0 64v240c0 17.673 14.327 32 32 32h64c11.842 0 22.175-6.438 27.708-16h7.052c19.146 16.953 46.013 60.653 68.76 83.4 13.667 13.667 10.153 108.6 71.76 108.6 57.58 0 95.27-31.936 95.27-104.73 0-18.41-3.93-33.73-8.85-46.54h36.48c48.602 0 85.82-41.565 85.82-85.58 0-19.15-4.96-34.99-13.73-49.84zM64 296c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24zm330.18 16.73H290.19c0 37.82 28.36 55.37 28.36 94.54 0 23.75 0 56.73-47.27 56.73-18.91-18.91-9.46-66.18-37.82-94.54C206.9 342.89 167.28 272 138.92 272H128V85.83c53.611 0 100.001-37.82 171.64-37.82h37.82c35.512 0 60.82 17.12 53.12 65.9 15.2 8.16 26.5 36.44 13.94 57.57 21.581 20.384 18.699 51.065 5.21 65.62 9.45 0 22.36 18.91 22.27 37.81-.09 18.91-16.71 37.82-37.82 37.82z"></path>
                </svg>
              </div>
              <div>{item.dislikes}</div>
            </div>
            <div>
              <span className={styles["feature-secondary"]}>Rating: </span>{" "}
              {item.rating}{" "}
              <span className={styles["feature-secondary"]}>Year: </span>{" "}
              {item.year}{" "}
              <span className={styles["feature-secondary"]}>Genres: </span>
              {item.genres.join(", ")}
            </div>
            <p>{item.summary}</p>
          </div>
          <div style={style}></div>
        </div>
      </div>
    </div>
  );
}

function Card({ setShowFeature, setItemFeature, items, isOpen = false }) {
  const [isCardOpen, setIsCardOpen] = useState(isOpen);
  if (!items.length) {
    return null;
  }
  return (
    <div>
      <h2
        className={styles["card-title"]}
        onClick={() => {
          setIsCardOpen((state) => !state);
        }}
      >
        {items.genre}
      </h2>
      <div
        className={`${styles["card-items"]} ${
          isCardOpen ? styles["card-items-active"] : ""
        }`}
      >
        {items.map((el) => (
          <CardItem
            item={el}
            key={el.movieId}
            setShowFeature={setShowFeature}
            setItemFeature={setItemFeature}
          />
        ))}
      </div>
    </div>
  );
}

function CardItem({ item, setItemFeature, setShowFeature }) {
  return (
    <div
      className={styles["card-item"]}
      onClick={() => {
        setItemFeature(item);
        setShowFeature(true);
      }}
    >
      <h3>{item.name}</h3>
      {item.image.medium && (
        <img className={styles.image} src={item.image.medium} alt={item.name} />
      )}
    </div>
  );
}
